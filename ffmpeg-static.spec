Name:           ffmpeg-static
Version:        6.0
Release:        1%{?dist}
Summary:        Repack of FFmpegstatic build by John Van Sickle

License:        GPLv3

%ifarch x86_64
%global archstr amd64
%endif
%ifarch aarch64
%global archstr arm64
%endif

URL:            https://johnvansickle.com/ffmpeg/
Source0:        https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-%{archstr}-static.tar.xz
Source1:        https://johnvansickle.com/ffmpeg/releases/ffmpeg-release-%{archstr}-static.tar.xz.md5

BuildRequires: tar, xz

Provides: ffmpeg
Suggests: nscd

%description
Repack of FFmpeg static build by John Van Sickle.

%package model
Summary: Required model by vmaf filter.
%description model
The models in FFmpeg static build by John Van Sickle.


%global debug_package %{nil}

%prep
%setup -n ffmpeg-%{version}-%{archstr}-static

%install
install -d %{buildroot}%{_bindir}

install -p -m 0755 ffmpeg %{buildroot}%{_bindir}/ffmpeg
install -p -m 0755 ffprobe %{buildroot}%{_bindir}/ffprobe

install -d %{buildroot}/usr/local/share
cp -r model %{buildroot}/usr/local/share/

%files
%{_bindir}/ffmpeg
%{_bindir}/ffprobe

%files model
/usr/local/share/model/*

%changelog
